
var bind = function(elem, evt, f){ return add_event(elem, evt, function(e){ return f.apply(this, arguments) }) };
var mkbind = function(elem, evt){ return function(f){ return bind(elem, evt, f) }};

var add_event = (function(){
	var mk = function(g,h){ return function(obj,ev,f,capt){
		if(!f)return;
		if(/^on/.test(ev)) ev=ev.substr(2);
		var rec = function(){
			var stop = function(){h.call(obj,ev,_f,capt)}
			var _f = function(e){
				if(!e) {
					e=window.event;
					e.target = e.srcElement;
				}
				return f.call(this,e,stop,rec);
			};
			g.call(obj,ev,_f,capt);
			return stop;
		};
		return rec();
	}};
	if(window.addEventListener)
		return mk(function(ev,f,capt){this.addEventListener(ev,f,capt)},
				  function(ev,f,capt){this.removeEventListener(ev,f,capt)});
	if(window.attachEvent)
		return mk(function(ev,f){this.attachEvent('on'+ev,f)},
				  function(ev,f){this.detachEvent('on'+ev,f)});
	return mk(function(ev,f){this['on'+ev]=f},
	          function(ev,f){delete this['on'+ev]});
})();


var add_one = function(elem, ev, f){
	return add_event(elem, ev, function(e, stop){ stop(); return f.apply(this, arguments) });
};


var stop_and_prevent = function(e, f){ e.stopPropagation();e.preventDefault(); is_function(f)&&f(); return !1 };


var mkevent = function(default_func){
	var rm=[], events={};
	var add0 = function(elem, ev, func, capt){
		var _this = this;
		if(!func) func=default_func;
		var f = function(e){
			if(_this.locked) {
				e.stopPropagation();
				e.preventDefault();
				return false;
			}
			var ret = func.apply(e.target, arguments);
			if(ret===false) _this.lock();
			if(_this.locked) {
				e.stopPropagation();
				e.preventDefault();
				return false;
			}
			return ret;
		};
		if(!events[ev]) events[ev]=[];
		(function(n, ev, m){
			events[ev][m] = n;
			rm[n] = function(){
				rm[n] = undefined;
				events[ev][m] = undefined;
				return elem.removeEventListener(ev, f, capt);
			};
		})(rm.length, ev, events[ev].length);
		if(elem.addEventListener)
			return elem.addEventListener(ev, f, capt);
		if(elem.attachEvent)
			return elem.attachEvent('on' + ev, f);
	}
	return {
		e: null,
		locked: false,
		lock: function(){
			//set_message('locked');
			var _this = this;
			this.locked = true;
			window.setTimeout(function(){_this.locked = false },0);
		},
		add: function(elem, evs, func, capt) {
			var m=evs.match(/(on)?\w+/);
			//alert([evs, dump(m)]);
		
			for(var i=0,evs=evs.split(/[, ]+/); i<evs.length; ++i)
				add0.call(this, elem, evs[i], func, capt);
		},
		remove: function(evs){
			if(evs) {
				var xx;
				for(var i=0,evs=evs.split(/[, ]+/); i<evs.length; ++i) {
					xx=events[evs[i]];
					if(!xx) continue;
					for(var i=0; i<xx.length; ++i)
						xx[i] && rm[xx[i]] && rm[xx[i]]();
				}
			} else {
				for(var i=0; i<rm.length; ++i)
					rm[i]();
				rm=[];
			}
			//this.locked=false;
			//this.e=null;
		},
		bind: function(){ return this.add.apply(this, arguments) },
		unbind: function(){ return this.remove.apply(this, arguments) }
	};
};


var get_keystroke_name=(function(){
	var LAT='QWERTYUIOP{}ASDFGHJKL:"ZXCVBNM<>';
	var RUS='ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ';
	var keys = [];
	keys[ 8]="BS";
	keys[ 9]="TAB";
	keys[13]="ENTER";
	keys[16]="SHIFT";
	keys[17]="CTRL";
	keys[18]="ALT";
	keys[27]="ESC";
	keys[32]="SPACE";
	keys[33]="PGUP";
	keys[34]="PGDN";
	keys[35]="END";
	keys[36]="HOME";
	keys[37]="LEFT";
	keys[38]="UP";
	keys[39]="RIGHT";
	keys[40]="DOWN";
	keys[45]="INS";
	keys[46]="DEL";
	return function get_event_keystroke_name(e){
		var x = e.charCode, y=e.keyCode;
		var mod1 = (e.ctrlKey ? 'CTRL_' : '') + (e.altKey ? 'ALT_' : '');
		var mod2 = e.shiftKey ? 'SHIFT_' : '';
		var mod3 = mod1 ? mod1 + mod2 : '';
		if(y>=112 && y<=123) return mod1 + mod2 + 'F' + (y-111);
		if(y && keys[y]) return mod1 + mod2 + keys[y];
		if(!x) x=y;
		if(!x) return '';
		if((x>=48 && x<=57) || (x>=65 && x<=90)) return mod3 + String.fromCharCode(x);
		if (x>=1040 && x<=1103) return mod3 + LAT.charAt(RUS.indexOf(String.fromCharCode(x>=1072 ? x-32 : x)));
		if (x>=97 && x<=122) return mod3 + String.fromCharCode(x-32);
		if(x==32) return mod1 + mod2 + 'SPACE';
		return mod1 + String.fromCharCode(x);
	};
})();



var add_keystroke = (function(){
	var event = mkevent();
	return function(ks, f){
		event.add(document, 'keypress keydown', function(e){
			if(get_keystroke_name(e)==ks) {
				event.lock();
				return f.apply(e.target, arguments);
			}
		});
	}
})();



var add_monitor = function(x, handler) {
	var f = is_function(x) ? x : new Function("return " + x);
	var v0 = f();
	return [ window.setInterval(function() {
		var v = f();
		if (v === v0) return;
		handler(v, v0);
		v0 = v;
	}, 200) ];
};


var remove_monitor = function(mon) {
	if (mon[0]) {
		window.clearInterval(mon[0]);
		delete mon[0];
	}
};










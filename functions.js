
/**
 * Implementation of Object.getOwnPropertyNames for
 * browsers that do not have this method.
 *
 * See https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Object/getOwnPropertyNames
 */
if (!Object.getOwnPropertyNames)
Object.getOwnPropertyNames = function(x) {
	var out = [];
	for (var i in x)
		if (x.hasOwnProperty(i))
			out[out.length] = i;
	return out;
};


/**
 * Functions for type detection. Fast implementation.
 */
var is_function = function(x){ return typeof x == 'function' };
var is_number   = function(x){ return typeof x == 'number' || (typeof x == 'object' && !!x.toPrecision) };
var is_string   = function(x){ return typeof x == 'string' || (typeof x == 'object' && !!x.charCodeAt) };
var is_date     = function(x){ return typeof x == 'object' && !!x.getFullYear };
var is_object   = function(x){ return typeof x == 'object' && x.constructor === Object || x instanceof Object };


/**
 * Check if value is array. 
 * It returns true for arrays, arguments and DOM collections.
 */
var is_array = (function(){
	if (Array.isArray)
		return function(x) {
			if (Array.isArray(x))
				return true;
			if (typeof x != 'object')
				return false;
			if (is_string(x))
				return false;
			return x.hasOwnProperty('length') && x.hasOwnProperty(0)
				&& x.hasOwnProperty(x.length - 1);
		};
	else
		return function(x) {
			if (typeof x != 'object')
				return false;
			if (is_string(x))
				return false;
			return x.hasOwnProperty('length') && x.hasOwnProperty(0)
				&& x.hasOwnProperty(x.length - 1);
		}
})();






/**
 * Evaluates lazy value.
 */
var getval = function(x){ return is_function(x) ? x() : x };


/**
 * Generates function that returns first element from 
 * array for which some checker returns true.
 */
var mk_getfirst = function(f, notfound) {
	return function(){
		for (var i = 0; i < arguments.length; ++i)
			if (f(arguments[i]))
				return arguments[i];
		return getval(notfound);
	}
};

var get_function = mk_getfirst(is_function, function(){});
var get_object   = mk_getfirst(is_object, {});
var get_array    = mk_getfirst(is_array, []);




/**
 * Returns function late that executes another function after timeout.
 *
 * Let var late = mklate(500);
 *
 * Then we can call some our function with delayed execution:
 * If we wrire late(f), then f will be execute after 500 ms., if no other 
 * call to late() was made. If we write late(100, f) then f will executes 
 * after 100 ms.
 *
 * If late was called multiple times in a small period then last call will be
 * applied. It late called without parameters then timer clears and delayed
 * execution aborts.
 *
 * In this example you move mouse over document, and after 0.5 sec 
 * after mouse was stopped message 'mousemove' alerts.
 * It you click on document after mouse was stopped and before message
 * was alerted then alert cancels;
 *
 *   var late = mklate(500);
 *   document.onmousemove = late(function() { alert('mousemove') });
 *   document.onclick = late();
 *
 */
var mklate = function(delay) {
	var is_function = function(x){ return x instanceof Function };
	var f, timer;
	var locked;
	var _this, _args;
	return function(d,func) {
		if (!d && timer) {
			window.clearTimeout(timer);
			timer = null;
			return;
		}
		if (typeof d == 'function') {
			func = d;
			d = undefined;
		}
		if (typeof(func) != 'function')
			func = function(){};
		return function(/*...*/){
			if (locked) return;
			if (this.constructor === Number) {
				d = this.valueOf();
				_this = null;
			} else
				_this = this;
			_args = arguments;
			if ((delay !== undefined) && (d === undefined))
				d = delay;
			if ((d !== undefined) && timer) {
				window.clearTimeout(timer);
				timer = null;
			}
			if (timer) return;
			timer = window.setTimeout(function(){
				timer = null;
				func.apply(_this, _args)
			}, d || 100);
		}
	};
};
		
var late = mklate();



/**
 * Generates function that will return instantiated template.
 */
var mktemplate = function(tpl, options){
	if (!tpl) return;
	options = objoin({
		left: "{%",
		right: "%}"
	}, options);
	tpl = tpl.replace(/&amp;/g, '&');
	tpl = tpl.replace(/;[ \t\r\n]*(@@)/g, '$1');
	tpl = tpl.replace(/\s+</mg, '<');
	tpl = tpl.replace(/\s+/g, ' ');
	var re = new RegExp(re_escape(options.left) + "|@@|" + re_escape(options.right), "g");
	var f_and = function(x) {
		return '\t(function(){return [' +
			bimap(
				function(x){ return quote(x) },
				function(x){ return x }
			)(x.split(re)).join(',\n\t\t') +
		'\n\t]}).apply(this)';
	};
	var f_or = function(x){
		var y = map(f_and)(x.split(/\s*@@\s*AND\s*@@\s*/g));
		return 'try{return [].concat(\n' + y.join(',\n') + ').join("")\n}catch(e){}';
	};
	var y = map(f_or)(tpl.split(/\s*@@\s*OR\s*@@\s*/g));
	var z = y.join(';\n');
	var f = new Function(z);
	return function(){
		if (this[0]) { // .constructor=='Array') {}
			var out = [];
			for (var i = 0; i < this.length; ++i)
				out[out.length] = f.apply(this[i]);
			return out.join('');
		}
		return f.apply(this);
	}
};



/**
 * Returns base name of parameter.
 */
var basename = function(str){
	if (typeof str != 'string')
		str = str.hasOwnProperty('pathname') ? str.pathname : '' + str;
	var l, r, h;
	r = str.indexOf('?');
	if (r < 0)
		r = str.indexOf('#');
	else {
		h = str.indexOf('#');
		if (h >= 0 && h < r)
			r = h;
	}
	if (r < 0) {
		l = str.lastIndexOf('/');
		return l < 0 ? str : str.substr(l + 1);
	}
	l = str.lastIndexOf('/', r);
	return l < 0 ? str.substr(0, r) : str.substr(l + 1, r - l - 1);
};



/**
 * Pass each odd element in array through one function,
 * and each even element through another function.
 * Usage: bimap(f, g)(ar).
 * @param f odd callback.
 * @param g even callback.
 * @param ar array.
 * @return new array.
 */
var bimap = function(f, g) {
	return function(ar) {
		var i, out = [];
		for (i = 0; i < ar.length; ++i)
			out[out.length] = i % 2 ? g(ar[i]) : f(ar[i])
		return out;
	};
};


/**
 * Returns double quoted value of escaped string.
 */
var quote = function(x) {
	return JSON.stringify('' + x);
};


/**
 * Escaped javascript checial characters in string.
 */
var js_escape = function(x) {
	var quoted_str = JSON.stringify('' + x);
	return quoted_str.substr(1, quoted_str.length - 2);
};


/**
 * Converts html special characters to html entities.
 */
var html_escape = function(x) {
	return ('' + x).replace(/&|<|>/g, function($0) {
		switch ($0) {
			case '&': return '&amp;';
			case '<': return '&lt;';
			case '>': return '&gt;';
		}
	});
};


/**
 * Escaped regular expressions special characters.
 */
var re_escape = function(x) {
	return String(x).replace(/\\/g, '\\\\').replace(/\n/g, '\\n').replace(/([\|\{\}\[\]\$\^\*\(\)\+\-\?\.])/g, '\\$1');
};



/**
 * Returns first argument which is not undefined and not null.
 */
var coalesce = function() {
	for(var i = 0, max = arguments.length; i < max; ++i)
		if (arguments[i] !== undefined && arguments[i] !== null)
			return arguments[i];
	return null;
};


/**
 * Apply a function against an accumulator and each 
 * value of the array (from left-to-right) as to reduce 
 * it to a single value.
 */
var fold = function(f) {
	return function(accum, param1, param2) {
		return function(ar) {
			var value;
			for (var i = 0, max = ar.length; i < max; ++i) {
				value = ar[i].valueOf();
				if (value !== undefined)
					a = f(value, accum, param1, param2);
			}
			return a;
		}
	}
};


/**
 * Creates object from array.
 * Example:
 *   trans({}, 1)(['a', 'b', 'c']) will return {a:1, b:1, c:1}
 */
var trans = fold(function(x, a, b){ a[x] = b; return  a });



/**
 * Iterates array elements or object properties, calls callback
 * and if it returns value, saves it in resulting array.
 */
var map = function(f){
	var map_array = function(ar){
		var x, out = [];
		for (var i = 0, max = ar.length; i < max; ++i) {
			x = f(ar[i], i, ar);
			if (x !== undefined)
				out[out.length] = x;
		}
		return out;
	};
	var map_object = function(obj) {
		var i, x, out = [];
		for (i in obj) {
			x = f(obj[i], i, obj);
			if (x !== undefined)
				out[out.length] = x;
		}
		return out;
	};
	return function(x) {
		if (!x) return;
		if (is_array(x)) return map_array(x);
		if (is_object(x)) return map_object(x);
		return f(x);
	}
};


/**
 * Copies properties of one object to another recursively.
 * If property names are the same, then if property value
 * is not array it creates array with property value and
 * adds property value of second object.
 */
var obcat = function(dst /*, obj1, obj2, ... */) {
	var rec = function(dst, src) {
		if (!(dst instanceof Object))
			dst = Object(dst);
		var i, x, s, d;
		if (typeof(src) != 'object')
			return dst;
		for (i in src) {
			if (i in src.constructor.prototype)
				continue;
			s = src[i];
			if (s === undefined)
				continue;
			if (dst[i] === undefined)
				dst[i] = s;
			else if (is_array(dst[i]) && is_array(s))
				dst[i] = dst[i].concat(s);
			else if (is_object(dst[i]) && is_object(s))
				rec(dst[i], s);
			else
				dst[i] = [].concat(dst[i], src[i]);
		}
		return dst;
	}
	var objs = Array.prototype.slice.apply(arguments);
	for (var i = 1, max = objs.length; i < max; ++i)
		rec(dst, objs[i]);
	return dst;
};


/**
 * Copies properties of one object to another recursively.
 * If property names are the same, then second object
 * overwrites first.
 */
var objoin = function(dst /*, obj1, obj2, ... */) {
	var rec = function(dst, src) {
		if (!(dst instanceof Object))
			dst = Object(dst);
		var i, x, s, d;
		if (typeof(src) != 'object')
			return dst;
		for (i in src) {
			if (i in src.constructor.prototype)
				continue;
			s = src[i];
			if (s === undefined)
				continue;
			if (dst[i] === undefined)
				dst[i] = s;
			else if (is_array(dst[i]) && is_array(s))
				dst[i] = dst[i].concat(s);
			else if (is_object(dst[i]) && is_object(s))
				rec(dst[i], s);
			else
				dst[i] = s;
		}
		return dst;
	}
	var objs = Array.prototype.slice.apply(arguments);
	for (var i = 1, max = objs.length; i < max; ++i)
		rec(dst, objs[i]);
	return dst;
};


/**
 * Formats string like C function sprintf do.
 * It string contains formatting and no arguments was given
 * then it returns function on that arguments then returns
 * formatted string with that arguments.
 * It supports special format, for example %1 that means
 * first arguments. %2 is second argument ...
 *
 * Example:
 *   sprintf('n=%02d', 5) will return '05'
 *   sprintf('n=%02d') will return function f and f(5) will return '05'
 *   sprintf('%1%1%1', 5) will return '555'
 */
var sprintf = (function(){
	var BLANKS = '                                                                                             ';
	var ZEROES = '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000';
	var bb = [];

	var format_string = function(x, size, prec, opt) {
		if (!is_string(x))
			x = '' + x;
		if (prec && (x.length - prec > 0))
			x = x.substr(0, prec - 3) + '...';
		if (!size || ((size -= x.length) < 0))
			return x;
		if (opt === '0')
			return ZEROES.substr(0, size) + x;
		if (opt === '-')
			return x.concat(bb[size] || (bb[size] = BLANKS.substr(0, size)));
		//alert(x.length);
		return (bb[size] || (bb[size] = BLANKS.substr(0, size))).concat(x);
	};
	
	var format_float = function(x, size, prec, opt, dot) {
		var y = undefined === prec ? '' + parseInt(x) : (+x).toFixed(prec);
		if (dot && (dot != '.') && +prec > 0)
			y = y.replace('.', dot);
		if (opt == '+' && +x > 0)
			y = '+' + y;
		return format_string(y, size, false, opt);
	};
	
	var format_human_readable = function(x, size, prec, opt){
		var u = '', p = 1;
		if (x > 1000000) {
			x /= 1000000;
			u = 'M';
			p = 1;
		} else if (x > 1000) {
			x /= 1000;
			u = 'K';
			p = 2;
		}
		return format_float(x, size, prec || p, opt) + u;
	};
	
	return function(s){
		var args = Array.prototype.slice.apply(arguments);
		var n = 0, last_argno = 0;
		if (typeof(s) != 'string')
			return is_function(window.dump) ? dump(s) : ('' + s);
		if ((args.length == 1) && /%[-+0]?[0-9\.]*[sfdihj$oO%]/.test(s)) {
			var f = function(){
				if (arguments.length > 0)
					return sprintf.apply(this, Array.prototype.concat.apply(args, arguments));
				return s;
			};
			f.toString = f;
			return f;
		}
		return s.replace(/%%|%([-+0])?([0-9]*)(?:([\.,])([0-9]*))?([\$_a-zA-Z])?/g,
			function($0, opt, par1, dot, prec, fmt) {
				if ($0 == '%%')
					return '%';
				if (par1 && !fmt && (+par1 < args.length))
					return args[par1];
				if (!fmt)
					return $0;
				var x = args[++last_argno];
				switch (fmt) {
					default: return $0;
					case 's': return format_string(x, par1, prec, opt);
					case 'q': return format_string('\'' + js_escape(x) + '\'', par1, prec, opt);
					case 'Q': return format_string('\"' + js_escape(x) + '\"', par1, prec, opt);
					case 'f': return format_float(x, par1, prec, opt, dot);
					case 'd':
					case 'i': return format_float(x, par1, prec, opt);
					case 'h': return format_human_readable(x, par1, prec, opt);
					case 'j':
					case '$': 
						try {
							return $(x).clone().wrap('<div>').html().replace(/^\s+/mg,'');
						} catch (e){
							return dump(x, 4, false);
						}
					case 'o': return dump(x, 4, false);
					case 'O': return dump(x, 4, true);
					case '%': return '%';
					//case 't': return coalesce(rian.text[x],x);
				}
			}
		);
	};
})(); // end of sprintf
	


/**
 * Generated unique identifier.
 */
var newid = function(prefix) {
	if (!prefix) prefix = 'id';
	return prefix + String(Math.random()).substr(2);
};



/**
 * Generates superposition of functions. 
 */
var o = function(/* ... */){
	if (!arguments[arguments.length - 1])
		return null;
	var ff = Array.prototype.slice.apply(arguments);
	var ok = [];
	var f = function(){
		for (var i = ff.length - 1; i >= 0; --i)
			arguments[0] = ff[i].apply(this, arguments);
		for (var i = 0; i < ok.length; ++i)
			ok[i].apply(this);
		return arguments[0];
	};
	f.ok = function(f){ ok[ok.length] = f };
	return f;
};



/**
 * Returns substring count in string.
 */
var substr_count = function(pat, text) {
	for (
		var i = text.length, n = 0;
		i > 0 && (i = text.lastIndexOf(pat, i - 1)) >= 0;
		++n
	);
	return n;
};


/**
 * Constructs new string from given string repeated given number times.
 */
var str_repeat = function(str, n) {
	var out = [];
	for(var i = 0; i < n; ++i)
		out[out.length] = str;
	return out.join('');
};



/**
 * to be documented 
 */
var mklazy = (function(){
	var global_time = 1;
	return function(options){
		var time = 1;
		var lazy = function(get, set){
			var vv = {}, v, t = 0;
			switch (get.arity) {
				default:
					return function(y){
						if ((t < time) || !v || (y !== undefined)) {
							t = time;
							if (y === undefined)
								v = get.apply(this, arguments);
							else {
								v = y;
								set && set.apply(this, arguments);
							}
						}
						return v;
					};
				case 1:
					return function(x, y){ 
						if (t < time || !vv[x] || (y !== undefined)) {
							t = time;
							if (y === undefined)
								vv[x] = get.apply(this, arguments);
							else {								
								vv[x] = y;
								set && set.apply(this, arguments);
							}	
						}
						return vv[x];
					};			
			}
		};
		lazy.reset = function(f){
			if (f) lazy.reset = o(f, arguments.callee);
			++time;
		};
		lazy.touch = mklate(1)(function(){lazy.reset()});
		return lazy;
	};
})();


/**
 * Formats string and alerts it.
 */
var printf = function(){
	var text = sprintf.apply(this, arguments);
	alert(text);
};


/**
 * conv(text, re, char | callback)
 *
 * Ищет по рег. выражению и заменяет вхождения на конструкции вида cccXXXccc, где 
 *   с - это символ, один из ~!@#$%^&*_+=|/ (по умолчанию - @),
 *   X - это цифра.
 *
 * Полученный текст может модифицироваться, после чего закодированные конструкции
 * могут быть восстановлены вызовом unconv.
 *
 */
//
// Пример использования:
//   x = text;
//   x = conv(x, /\/\*.*?\*\/|\/\/[^\n]*/, '*');
//   x = conv(x, /(['\/"])(\\.|[^\1])*?\1/, '/');
//   x = conv.unconv(x);
//   alert(x == text); 
//
var mkconv = function(){
	var dict = [];
	var conv = function(text, pat, f) {
		if (text && text.constructor === RegExp) { _text = text; text = pat; pat = _text; }
		if (!f)
			f = 0;
		var conv_left = '@@@';
		var conv_right = '@@@';
		if (is_string(f)) {
			if (f.length == 1) {
				conv_left = conv_right = f + f + f;
				f = 0;
			} else
				f = (function(repl){ return function(){ return repl } })(f);
		}
		if (is_number(f))	
			f = (function(n){ return function(){ return arguments[n] } })(f);

		var flags = 'g';
		if (pat.constructor === RegExp) {
			if (pat.ignoreCase)
				flags += 'i';
			pat = ('' + pat).replace(/^\/(.*)\/[igm]*$/, '$1');
		}
		pat = pat.replace(/\\n/g, '\x01');
		pat = pat.replace(/\\r/g, '\x02');
		
		var re = new RegExp('(?!\\$)' + pat, flags);
		text = text.replace(/\n/g, '\x01');
		text = text.replace(/\r/g, '\x02');
		text = text.replace(re, function(){
			var id = dict.length;
			dict[id] = f.apply(this, arguments);
			return conv_left + ('' + (1000 + id)).substr(1) + conv_right;
		});
		text = text.replace(/\x01/g, '\n');
		text = text.replace(/\x02/g, '\r');
		return text;
	};
	conv.unconv = function(text, f){
		return text.replace(/([~!@#$%^&*_\+=|\/])\1\1(\d{3})\1\1\1/g, function($0, c, id){
			id = +id;
			var x = dict[id];
			delete dict[id];
			return f && f.call(this, x) || x;
		}).replace(/\x01/g, '\n').replace(/\x02/g, '\r');
	};
	conv.reset = function(){ dict = [] };
	return conv;
};
var conv = mkconv();


/**
 * Returns integer numbers sequense with given first and last element.
 */
var range = function(n, m) {
	var i, out = [], j = 0;
	n = +n;
	m = +m;	
	if (n < m)
		for (; n <= m; ++n)
			out[out.length] = n;
	else
		for (; n >= m; ++n)
			out[out.length] = n;
	return out;
};


/**
 * Returns sequense of natural numbers with given length.
 */
var seq = function(n){
	return range(1, n)
};


/**
 * Expands string like bash.
 *
 * "x{A,B}"     -> "xA xB"
 * "{x,y}{A,B}" -> "xA xB yA yB"
 * "{1..3}"  is same as "{1,2,3}"
 * "{2..-1}" is same as "{2,1,0,-1}"
 *
 */
var expand_range = function(str, bDelimited) {
	var expand_range0 = function(str) {
		var re = /{(-?\d+)\.\.(-?\d+)}|{([^{}]+)|(\\\\|\\{|[^{}])+/g;
		var x0, x1, out, i, j;
		var out = [], m, r1, r2, i, list;
		while (m = re.exec(str)) {
			if (m[1] !== undefined) {
				list = [];
				r1 = +m[1];
				r2 = +m[2];
				
				if (r1 <= r2 )
					for (i = r1; i <= r2; ++i)
						list[list.length] = i;
				else
					for (i = r1; i >= r2; --i)
						list[list.length] = i;
						
				out[out.length] = list;
				continue;
			}	
			if (m[3] !== undefined) {
				out[out.length] = m[3].split(/,/);
				continue;
			}
			out[out.length] = m[0];
		}
		var out2 = [];
		var x = out;
		var n = 0, max_n = x.length;
		x0 = x[0];
		if (x0.constructor !== Array)
			x0 = [x0];
		for (n = 1; n < max_n; ++n) {
			x1 = x[n];
			if (x1.constructor !== Array)
				x1 = [x1];
			out = [];
			for (i = 0; i < x0.length; ++i)
				for (j = 0; j < x1.length; ++j)
					out[out.length] = [x0[i], x1[j]];
			x0 = out;
		}
		x = out;
		x.toString = function() {
			var xx = this;
			if (xx.length == 1 && is_array(xx[0]))
				xx = xx[0];
			var save = Array.prototype.toString;
			Array.prototype.toString = function() { return this.join('') };
			var out = xx.join(' ');
			Array.prototype.toString = save;
			return out;
		};	
		return x;
	};
	if (!bDelimited && /[^\\][ \t\r\n]/.test(str))
		return str.replace(/(\\\\|\\ |[^ \t\r\n])+/g, expand_range0);
	return expand_range0(str);		
};

// assert(expand_range('a{1..2} b{2..-2}c d{X,Y}')=='a1 a2 b2c b1c b0c b-1c b-2c dX dY')();


/**
 *  removes ' ', \n, \r, \t  from beginning end ending of given string.
 */
var trim = (function(){
	if (String.prototype.trim)
		return function(str) {
			return str.trim();
		};
	return function(str) {
		var i, j, max = str.length - 1, c;		
		for (i = 0; i <= max; ++i) {
			c = str.charAt(i);
			if (c == ' ' || c == '\t' || c == '\n' || c == '\r') continue;
			break;
		}		
		for (j = max; j > i; --j) {
			c = str.charAt(j);
			if (c == ' ' || c == '\t' || c == '\n' || c == '\r') continue;
			break;
		}
		if (j < max) return str.substr(i, j - i + 1);
		if (i) return str.substr(i);
		return str;
	};
})();
 



/**
 * Simple multiple search and replace.
 * replace_pairs is [ [ regexp | substr, newSubStr | function ] ... ]
 */
var tr = function(str, replace_pairs){
	str = '' + str;
	var i, max, x;
	for (i = 0, max = replace_pairs.length; i < max && (x = replace_pairs[i]); ++i)
		str = str.replace(x[0], x[1]);
	return str;
};


/**
 * Возвращает регулярное выражение, соответствующее данному css селектору.
 * Используется в функции get_element_path для проверки, соответствует ли
 * путь серектору.
 */
var get_selector_regexp = function(selector){
	var re_src = tr(trim(selector), [
		[/\\/g, 	'\\\\' 		],
		[/\./g, 	'~'			],
		[/ *, */g, 	'|'			],
		[/~/g, 		'\\.'		],
		[/^\.\*/, 	''			],
		[/ *, */g, 	'|'			],
		[/[\[\]]/g, '\\$&'		],
		[/ +/g, 	'[^ ]* .*'	],
		[/\b\w+/g, 	'\\b$&\\b'	]
	]);
	return new RegExp(re_src);
};


/**
 * Загружает файл в синхронном режиме.
 */
var load_file = function(uri){
	var xhr = new XMLHttpRequest;
	xhr.open('GET', uri, false);
	xhr.send(null);
	return xhr.responseText;
};


/**
 * Возвращает число, ближайшее к данному в заданном диапазоне.
 */
var minmax = function(x, min, max) {
	if (is_array(min)) min = Math.max.apply(Math, min);
	if (is_array(max)) max = Math.min.apply(Math, max);
	return Math.min(Math.max(x, +min), +max);
};


/**
 * Вычисляет среднее значение и абс. погрешность по массиву значений замеров,
 * значению сист. погрешности и функции.
 */
var stat = function(xx, syst, f) {
	var sqr = function(x) { return x * x };
	var sqrt = function(x) { return Math.sqrt(x) };
	var N = xx.length;	
	var i, s;
	for (s = 0, i = 0; i < N; ++i)
		s += xx[i];
	var X = s / N;
	for (s = 0, i = 0; i < N; ++i)
		s += (xx[i] - X) * (xx[i] - X);
	var S = Math.sqrt(s / (N - 1));
	var stud = [, , 12.7, 4.3, 3.2, 2.8, 2.6, 2.4, 2.4, 2.3, 2.3, 2.1, 2.1, 2.0, 2.0 ]
	var t095 = N < stud.length ? stud[N] : 2;
	var D= sqrt(sqr(syst || 0) + sqr(t095 * S));
	var E = D / X;
	if (f) {
		var df = derivative(f);
		var Y = f(X);
		D = Math.abs(D * df(X));
		E = D / Y;
		return [Y, D, E];
	}
	return [X, D, E];
};



/**
 * Вычисляет производную заданной функции в заданной точке.
 */
var derivative = function(f) {
	var eps = 0.00000000001;
	return function(x) {
		var n = 1, dy = 0, dx = 0;
		var cnt = 0;
		for (; dx < 0.01 && Math.abs(dy) < eps; n) {
			n *= 10;
			dx = eps * n;
			if (++cnt > 1000) {
				alert(['cnt > 1000!', x, n, dx, dy]);
				return;
			}
			dy = f(x + dx) - f(x);
		}
		var result = dy / dx;
		result = Math.round(result * n) / n;
		return result;
	};
};



/**
 * Три функции, округляющие число по заданной точности по правилам округления, 
 * сверху или снизу.
 *
 *   round(1234.5678, 1) = round(1234.5678) = 1235
 *   round(1234.5678, 0.1) = 1234.6
 *   round(1234.5678, 100) = 1200
 */ 
var floor = function(x, d) {
	if (d === undefined) return Math.floor(x);
	return d > 1 ? Math.floor(x / d) * d : Math.floor(x * (1 / d)) / (1 / d);
};
var round = function(x, d) { return d === undefined ? Math.round(x) : floor(x + d/2, d) };
var ceil  = function(x, d) { return d === undefined ? Math.ceil(x)  : floor(x + d,   d) };


/**
 * Returns random integer in given range.
 */
var rand = function(n, m) {
	return Math.round(Math.random() * Math.abs(n - m))
};


/**
 * Возвращает объект даты из строки в формате d.m.y h:i
 */
var parseDate = function(x) {
	if (!x) return;
	if (is_date(x))
		return x;
	var match = x.match(/^ *(\d{1,2})\.?(\d{2})\.?(\d{2}|\d{4})([^\d]+(\d+):(\d+) *)?$/);
	if (!match) return;
	var year = +match[3], month = +match[2] - 1, date = +match[1], hour = match[5] && +match[5], minute = match[6] && +match[6];
	if (year < 100)
		year += 2000;
	var d = new Date(year, month, date);
	if (hour)
		d.setHours(hour);
	if (minute)
		d.setMinutes(minute);
	switch (true) {
		case year  != d.getFullYear() :
		case month != d.getMonth()    :
		case date  != d.getDate()     :
			return;
	}
	return d;
};



/**
 * Возвращает объект даты со временем, равным нулю.
 */
var date_date = function(d) {
	return d ? new Date(d.getFullYear(), d.getMonth(), d.getDate()) : undefined;
};


/**
 * Возвращает объект даты со временем, округленным до заданного количества минут в 
 * меньшую сторону.
 */
var date_floor = function(d, prec) {
	if (!d) return;
	var r = new Date(d);
	r.setMinutes(prec * Math.floor(r.getMinutes() / prec));
	return r;
};	


/**
 * Возвращает объект даты со временем, округленным до заданного количества минут в 
 * большую сторону.
 */
var date_ceil = function(d, prec) {
	if (!d) return;
	var r = new Date(d);
	r.setMinutes(prec * Math.ceil(r.getMinutes() / prec));
	return r;
};



var dump_config = (function(){
	var config = {};
	return function(x) {
		if (x !== undefined)
			objoin(config, x);
		return config;
	}
})();



/**
 * Returns formatted plain text dump of the value.
 */
var dump = (function(){
	var _locked_refs = [];
	var config = dump_config({ html: true });
	
	var sprintf = function() {
		var args = arguments;
		if (!config.html) {
			for (var i = 1; i < args.length; ++i)
				if (is_string(args[i]))
					args[i] = args[i].replace(/<\/?.*?>/g, '');
		}
		return window.sprintf.apply(this, args);
	};
	
	
	var dump0 = function(x, prefix, max_depth, out, depth, path){			
		var max, cond, prefix2, delim, i, v=x, first;
		if (!path)
			path = 'R';
		
		//out[out.length] = 'z' + path + ':';
		//prefix += '  ';
		
		if (depth > 0) {
			if (x === document) {
				out[out.length] = '#document "' + basename(x.location.pathname) + '"';
				return;
			}
			if (x === window) {
				out[out.length] = '#window "' + basename(x.location.pathname) + '"';
				return;
			}
		}
		
		//if (v.hasOwnProperty('toString')) v = v.toString() + '>';
		switch(typeof v){
			default: return;
			case 'undefined':
				out[out.length] = 'undefined';
				return;
			case 'function':
				if (v.hasOwnProperty('toString'))
					out[out.length] = sprintf('<i>[func]</i> %s', v.toString());
				else {
					v = '' + v;
					out[out.length] = v.substr(0, v.indexOf('{')) + ' { ... }';
				}
				return;
			case 'string': 
				//out[out.length]='"' + v.replace(/[\n\r\t ]+/g, ' ') + '"';
				out[out.length] = '"' + js_escape(v) + '"';
				return;
			case 'number': case 'boolean': out[out.length]=v; return;
			case 'object':
				if(v===null) { out[out.length] = 'null'; return; }
				if(depth>max_depth) { out[out.length] = '"######"'; return; }
				//y{ if(v.__locked) return }catch(e){ out[out.length]='"FUCK"'; return }
				switch(v.constructor) {
					case Number:
					case Boolean:  out[out.length] = v; return;
					case Date:     out[out.length] = '"' + v + '"'; return;
					case String:   out[out.length] = '"' + v.replace(/[\n\r\t ]/g, '') +'"'; return;
				}
				break;
		}

		if (x.__locked) {
			out[out.length] = sprintf('<a href="#%s" style="color:blue;font-weight:bold">%s</a>', x.__locked, x.__locked);
			return;
		}
		
		try { x.__locked = path; _locked_refs[_locked_refs.length] = x; } catch(e) { return '"***CANNOT_LOCK***"' }
		
		//alert(is_object(x));
		if (!is_object(x) && !is_array(x)) {
			out[out.length] = '' + x;
			return;
		}
		
		if(!is_array(x)) {
			var ii = [];
			for(i in x) {
				if (!x.hasOwnProperty(i)) continue;
				if (i=='__locked') continue;
				if (i=='constructor') continue;
				//try { if(x[i].__locked) continue; } catch(e){continue}
				v = x[i];
				if (is_function(v) && !v.hasOwnProperty('toString')) continue;
				ii[ii.length] = i;
			}
			switch (ii.length) {
				case 0:
					out[out.length] = '{}';
					return;
				
				case 1:
					if (typeof x[ii[0]] != 'object') {
						out[out.length] = '{ ';
						out[out.length] = sprintf('<b>%s</b>: ', ii[0]);
						if(x[ii[0]].__locked)
							out[out.length] = x[ii[0]].__locked;
						else
							dump0(x[ii[0]], prefix + '    ', max_depth, out, depth + 1, path + '.' + ii[0]);
						out[out.length] = ' }';
						return;
					}
				
				default:					
					out[out.length] = '{';
					out[out.length] = '\n';
			}
			
			prefix2 = prefix!==false ? prefix + '    ' : false;
			delim = prefix===false ? ',' : ',\n';
			first = true;
			var p;
			
			for (var n = 0, nmax = ii.length; n < nmax; ++n) {
				i = ii[n];
				v = x[i];				
				if (!first)
					out[out.length] = delim;
				if (/\W/.test(i))
					out[out.length] = sprintf('%s    <b>%q</b>: ', prefix, i);
				else
					out[out.length] = sprintf('%s    <b>%s</b>: ', prefix, i);
					
				dump0(v, prefix2, max_depth, out, depth + 1, 
					sprintf( /\W/.test(i) ? '%s[%q]' : '%s.%s', path, i));
					
				first = false;
			}
			out[out.length] = '\n' + prefix;
			out[out.length] = '}';
			return;
		} else {
			if (!x.length) {
				out[out.length] = '[]';
				return;
			}
			
			max = x.length;
			cond = max > 8;
			if (is_string(x[0]) && (x[0].length > 50))
				cond = true;
			if (typeof x[0] == 'object')
				cond = true;
			cond = true;
			out[out.length] = cond && (prefix!==false) ? '[\n' + prefix + '    ' : '[';
			//prefix2 = prefix!==false ? prefix : false;
			prefix2 = prefix !== false ? prefix + '    ': false;
			delim = prefix === false ? ',' : ( cond ? ',\n' + prefix + '    ' : ', ');
			
			
			
			first = true;
			for (var i = 0, v; i < max; ++i){
				v = x[i];
				if (v === undefined) continue;
				if (!first)
					out[out.length] = delim;

				out[out.length] = sprintf('<a name="%s" style="color:#880">[%d] = </a>', path + '[' + i + ']', i);
				first = false;
				
				dump0(v, prefix2, max_depth, out, depth, path + '[' + i + ']');
				
			}
			if (cond)
				out[out.length] = '\n' + prefix;

			out[out.length] = ']';
			return;
		}
	}
	return function(x, title, indent) {
		var out = [];
		if (is_number(indent))
			indent = str_repeat('    ', indent);

		if (title)
			out[out.length] = sprintf('<h3 style="font-size:12px;margin:0;background-color:black;color:white">%s</h3>', title);
		
		out[out.length] = sprintf('<pre style="margin:0">');
		
		if (indent)
			out[out.length] = indent;
			
		dump0(x, indent || '', 20, out, 0);
		
		for (var i = 0, max = _locked_refs.length; i < max; ++i)
			delete _locked_refs[i].__locked;
		_locked_refs = [];

		out[out.length] = sprintf('</pre>');
		
		
		var save = Array.prototype.toString;
		Array.prototype.toString = function(){ return this.join('') };
		var x = out.join('');
		Array.prototype.toString = save;
		return x;
	}
})(); // dump



/**
 * returns unique values of array
 */
var uniq = function(xx) {
	var save_object_toString   = Object.prototype.toString;
	var save_array_toString    = Array.prototype.toString;
	var save_function_toString = Function.prototype.toString;
	var dict = [];

	Object.prototype.toString = 
	Array.prototype.toString = 
	Function.prototype.toString = 
		function() {
			if (!this.__unique_id)
				dict[this.__unique_id = dict.length] = this;
			return this.__unique_id;
		};

	var assoc = {}, out = [], x, i, max;

	for (i = 0, max = xx.length; i < max; ++i) {
		x = xx[i];
		if (!assoc[x]) {
			out[out.length] = x;
			assoc[x] = 1;
		}
	}

	Object.prototype.toString   = save_object_toString;
	Array.prototype.toString    = save_array_toString;
	Function.prototype.toString = save_function_toString;
	
	for (i = 0, max = dict.length; i < max; ++i)
		delete dict[i].__unique_id;
	return out;
};






	
if (!document.defaultView)
	document.defaultView = document.parentWindow;

var is_firefox  = function(){ return !!Function.toSource };
var is_ie       = function(){ return !!window.attachEvent };
var is_safari   = function(){ return /a/.__proto__=='//' };
var is_chrome   = function(){ return !!window.chrome };
var is_opera    = function(){ return !!window.opera };



//

ELEMENT_NODE	= 1;
TEXT_NODE		= 3;
COMMENT_NODE	= 8;
DOCUMENT_NODE	= 9;


var is_element = function(x){ return x instanceof Element || x instanceof Document };
var get_element  = mk_getfirst(is_element, function(){return document.createElement('div')});

var is_jquery = function(x) { return !!x.jquery };



var get_document_view = function(doc){ return doc.defaultView || doc.parent };
var get_window = function(doc){ return doc.defaultView || doc.parent };

/*
 *  
 */
var get = function(id,f){
	if(!id)return;
	if(id[0]=='#') id=id.substr(1);
	var x=document.getElementById(id);
	return f ? f.call(x, x) : x;
};



var get_zoom = function() {
	var computed_width = parseInt(get_style(document.documentElement, 'width'));
	var offset_width = document.documentElement.offsetWidth;
	var k = computed_width / offset_width;
	//return Math.round(100 * k) / 100;
	return k;
};


var get_style = function(elem, prop) {
	var s = window.getComputedStyle ? window.getComputedStyle(elem, '') : elem.runtimeStyle;
	if (!prop) return s;
	var prop_in_camel_case = prop.replace(/\-(.)/g, function($0, $1){ return $1.toUpperCase() });
	return s[prop_in_camel_case];
};




var element_css = function(elem, css){
	var out=[];
	var s = get_style(elem);
	for(var i in css) out[out.length] = i + ':' + css[i];
	out = out.join(';').replace(/[A-Z]/g, '-$&').toLowerCase();
	var cssText = elem.style.cssText + ';' + out.replace(/:\d+/g, '$&px');
	elem.style.cssText = cssText;
};


var element_popup = function(elem, content){
	var wrapper = document.createElement('div');
	var pos = get_position(elem);
	wrapper.style.cssText = sprintf("\
		zdisplay:   none;           \
		overflow:  auto;         \
		width:     auto;           \
		border:    2px solid green;\
		position:  absolute;       \
		z-index:   9999;           \
		background-color:white;    \
		top:       %dpx;           \
		margin-top:%dpx;           \
		left:      %dpx;           \
		height:    1px;           \
		width:     1px;           \
	", pos.top + elem.offsetHeight, elem.clientTop, pos.left, 200, document.width - pos.left);
	var id = String(Math.random()).substr(2);
	wrapper.id = id;
	wrapper.innerHTML = content;
	document.body.appendChild(wrapper);
	var width = wrapper.scrollWidth;
	var height = wrapper.scrollHeight;
	//if($ && $.fn && $.fn.animate)
	//	$(wrapper).animate({width:width, height:height}, 50);
	//else
		element_css(wrapper, {width:width, height:height});
	
	//add_one(window, 'mousedown', function(e){
	//	wrapper.style.display='none';
	//});
	
	return;
	var w = wrapper.scrollWidth;
	var dw = wrapper.offsetWidth - wrapper.clientWidth;
	wrapper.style.left = document.width - w + 'px';
	wrapper.style.width = w - dw + 'px';
	return wrapper;
};

//var GRID = sprintf('background:transparent url("data:image/gif;base64,R0lGODlhAgACAIABAAAAAFUAACH5BAEKAAEALAAAAAACAAIAAAIDDBAFADs=") repeat;');

var create_rectangle = function(x, y, w, h) {
	var ABS = sprintf('left:%dpx;top:%dpx;width:%dpx;height:%dpx;position:absolute;%s');
	var div = document.createElement('div');
	div.style.cssText = ABS(x, y, w, h, 'z-index:-1;border:4px solid #aaa;');
	return document.body.appendChild(div);
};

var remove_element = function(elem){
	elem && elem.parentNode && elem.parentNode.removeChild(elem);
};


var get_element_real_parent = (function(){
	var get_real_parent = function(x){
		if(!x.parentNode.parentNode)
			return x.ownerDocument && x.ownerDocument.defaultView && x.ownerDocument.defaultView.frameElement;
		return x.parentNode;
	};
	return function(p){
		while(p=get_real_parent(p)){
			if(!('tagName' in p))continue;
			if(p.tagName=='HTML')continue;
			return p;
		}
		return null;
	};
})();


var get_element_path_token = function(x){
	var attrs = (function(name){
		return name ? '[name=' + re_escape(name) + ']' : '';
	})(x.getAttribute('name'));
	return x.tagName.toLowerCase()+(x.id ? '#'+x.id : '') + x.className.replace(/([^ ]+) */g, '.$1') + attrs;
};

var get_element_path = function(elem){
	var get_real_parent = function(x){
		if(!x || !x.parentNode) return null;
		if(!x.parentNode.parentNode)
			return x.ownerDocument && x.ownerDocument.defaultView && x.ownerDocument.defaultView.frameElement;
		return x.parentNode;
	};
	var out=[], p=elem;
	do{
		if(!p)break;
		if(!('tagName' in p))continue;
		if(p.tagName=='HTML')continue;
		out[out.length] = get_element_path_token(p);
	}while(p=get_real_parent(p));
	out.reverse();
	return out.join(' ');
};

var get_element_geometry1 = function(elem, root){
	var p=elem, x=-elem.clientLeft, y=-elem.clientTop;
	if (!root) root=document;
	do{
		do{
			x+=p.offsetLeft;
			y+=p.offsetTop;
			if(get_style(p,'position')=='fixed'){
				x+=p.ownerDocument.documentElement.scrollLeft;
				y+=p.ownerDocument.documentElement.scrollTop;
			}
		} while (p.offsetParent && (p=p.offsetParent) && (p!=root))
		if((p=p.ownerDocument)==root) break;
		x-=p.defaultView.scrollX;
		y-=p.defaultView.scrollY;
	}while((p=p.defaultView.frameElement) && (p!=root));
	x += elem.clientLeft;
	y += elem.clientTop;
//	var g = function(f){return f( XXX
	var g = {};
	g.x = g.left = x;
	g.y = g.top = y;
	g.w = g.width  = elem.offsetWidth;
	g.h = g.height = elem.offsetHeight;
	g.r = g.right  = x + g.w;
	g.b = g.bottom = y + g.h;
	return g;
};


var get_element_geometry = function(p, root){
	var x=0, y=0;
	var w = p.offsetWidth;
	var h = p.offsetHeight;
	var p2, p3;
	if (!root) root = document;
	do{
		do{
			if(get_style(p, 'position')=='fixed') {
				x+=p.ownerDocument.defaultView.pageXOffset;
				y+=p.ownerDocument.defaultView.pageYOffset;
			}
			x+=p.offsetLeft;
			y+=p.offsetTop;
			//y-=p.scrollTop;
			if((p3=p).offsetParent)
				while((p3=p3.parentNode) && (p3!=p.offsetParent)) {
					y -= p3.scrollTop;
					x -= p3.scrollLeft;
				}
					
			if(p.offsetParent && (p.offsetParent.tagName=='TD')) {
				p2=p.parentNode;
				while(p2!==p.offsetParent) {
					x-=p2.scrollLeft;
					y-=p2.scrollTop;
					p2=p2.parentNode;
				}			
			}
		} while (p.offsetParent && (p=p.offsetParent) && (p!==root));
		if((p=p.ownerDocument)===root) break;
		x-=p.defaultView.scrollX;
		y-=p.defaultView.scrollY;
		if(x<0)x=0;
		if(y<0)y=0;
	}while((p=p.defaultView.frameElement) && (p!==root));
	return {
		left: x,
		top: y,
		width: w,
		height: h,
		right: x + w,
		bottom: y + h
	};
};


var get_position = function(elem,root){
	return get_element_geometry.apply(this, arguments);
};

/*	
var get_position = function(elem,root){
    var p=elem, x=-elem.clientLeft, y=-elem.clientTop;
    if (!root) root=document;
    do{
        do{
            x+=p.offsetLeft;
            y+=p.offsetTop;
        } while (p.offsetParent && (p=p.offsetParent) && (p!=root))
        if((p=p.ownerDocument)==root)
            break;
        x=Math.max(0, x-p.defaultView.pageXOffset);
        y=Math.max(0, y-p.defaultView.pageYOffset);
    }while((p=p.defaultView.frameElement) && (p!=root));
    
    var pos = {
    	left: x+elem.clientLeft,
    	top: y+elem.clientTop,
    	width: elem.offsetWidth,
    	height: elem.offsetHeight
    };
    pos.right = pos.left + pos.width;
    pos.bottom = pos.top + pos.height;
    return pos;
};
*/    



/*
var mkis = (function(){
	var a = sprintf("return (typeof(x)=='object') && x.hasOwnProperty('%s')");
	var b = sprintf("return (typeof(x)=='object') && (x.%s==%s)");
	return function(prop, val) { return new Function('x', (val===undefined ? a : b)(prop, val)) };
})();

var is_window   = mkis('frameElement');
var is_document = mkis('nodeType', DOCUMENT_NODE);
var is_element  = mkis('nodeType', ELEMENT_NODE);
var is_textnode = mkis('nodeType', TEXT_NODE);
*/
	
var is_window   = function(x){ return (typeof x == 'object') && !!x.setTimeout };
var is_document = function(x){ return (typeof x == 'object') && x.nodeType === DOCUMENT_NODE };
var is_element  = function(x){ return (typeof x == 'object') && x.nodeType === ELEMENT_NODE };
var is_textnode = function(x){ return (typeof x == 'object') && x.nodeType === TEXT_NODE };


/*
var is_document = function(x){ return ('nodeType' in x) && x.nodeType==DOCUMENT_NODE };

var is_element  = function(x){ return ('nodeType' in x) && x.nodeType==ELEMENT_NODE };
var is_textnode = function(x){ return ('nodeType' in x) && x.nodeType==TEXT_NODE };

var is_window   = function(x){
	return (typeof(x)=='object') && x.hasOwnProperty('frameElement');
};
*/




//window.setTimeout(function(){
//	dom_hilite(document);
//},100);


var delete_elem = function(x){
	x.parentNode.removeChild(x);
};

var create_elem = function(x, attrs, style, next_child){
	if(typeof(attrs)=="string") style=attrs, attrs=null;
	if(!(x instanceof HTMLElement)) x=document.createElement(x);
	if(style) x.style.cssText=style;
	var classes=[];
	if(attrs){
		if(attrs['class'])
			classes[classes.length]=attrs['class'];
		if(attrs.classes) classes=classes.concat(attrs.classes);
		delete attrs.classes, attrs['class'], attrs.className;
		objoin(x, attrs);
	}
	x.className=classes.join(" ");
	if(next_child) x.appendChild(next_child);
	return x;
};



var create = create_elem;
var DIV = function(style, next_child) { return create_elem("div", null, style, next_child) };
var SPAN = function(style, next_child) { return create_elem("SPAN", null, style, next_child) };

var append_first = function(parent, tagname, attrs, style){
	var x = create_elem(tagname, attrs, style);
	parent = get(parent);
	if(!parent.firstChild)
		return parent.appendChild(x);
	return parent.insertBefore(x, parent.firstChild);
};

var append_last = function(parent, tagname, attrs, style, next_child){
	if(!tagname)
		tagname = "span";
	var x = create_elem(tagname, attrs, style, next_child);
	return parent.appendChild(x);
};

var append_child = append_last;
var append = append_last;

var insert_before = function(before, tagname, attrs, style){
	var x = create_elem(tagname, attrs, style);
	before = get(before);
	return before.parent.insertBefore(x, before);
};

var insert_after = function(after, tagname, attrs, style){
	var el = create_elem(tagname, attrs, style);
	var x = get(after).nextSibling;
	if(!x)
		return x.parent.appendChild(el);
	return x.parent.insertBefore(el, x);
};


var replace_node = function(old, x){
	return old.parentNode.replaceChild(x, old);
};


var swap_nodes = function(a, b){
	if(a==b)
		return;
	var tmp = document.createTextNode("");
	replace_node(a, tmp);
	replace_node(b, a);
	replace_node(tmp, b);
};



var write = function(elem, html, style){
	elem.innerHTML = html;
	if (style)
		elem.style.cssText += style;
};






var append_div = function(style){
	return append_last(document.body, "div", {}, style);
};



var add_js = (function(){
	var xx={};
	var rec = function(src, f, bAsync){
		if(!bAsync) bAsync=true;
		(function(ss){
			for(var x, i=0; x=ss.item(i) && ''+ss.item(i).getAttribute('src'); ++i)
				if(x==src) return xx[src]=1;
		})(this.document.getElementsByTagName('script'));
		if(xx[src]==1) return;
		if(!this.document.body) {
			this.document.write('<script src="'+src+'" type="text/javascript"><\/script>');
			xx[src] = 1;
			f&&window.setTimeout(f,0);
		} else if(f===false) {
			var req = new XMLHttpRequest;
			req.open('GET', src, false);
			req.send(null);
			var s = this.document.createElement('script');
			s.text = req.responseText;
			this.document.getElementsByTagName('head')[0].appendChild(s);
			s.setAttribute('src', src);
			xx[src]=1;
			
		} else {
			var s = this.document.createElement('script');
			s.setAttribute('type', 'text/javascript');
			s.setAttribute('src', src);
			f && add_one(s,'load', f);
			xx[src] = 1;
			this.document.getElementsByTagName('head')[0].appendChild(s);
		}
	};
	return function(srcs, f, bAsync){
		if(typeof(srcs)=='string') return rec.apply(this, arguments);
		if(f===false) bAsync=false;
		var g = f && function(){ if(++cnt>=max) return f&&f.apply(this,arguments) };
		var max = srcs.length, cnt=0;
		for(var i=0; i<max; ++i)
			rec.call(this, srcs[i], g, bAsync);
	};
})();


var add_css = (function(){
	var xx={};
	var rec = function(src){
		(function(ss){
			for(var x, i=0; x=ss.item(i) && ''+ss.item(i).getAttribute('href'); ++i)
				if(x==src) return xx[src]=1;
		})(this.document.getElementsByTagName('head')[0].getElementsByTagName('link'));
		if(xx[src]==1) return;
		if(!this.document.body) {
			this.document.write('<link rel="stylesheet" type="text/css" href="'+src+'" />');
			xx[src] = 1;
		} else {
			var s = this.document.createElement('link');
			s.setAttribute('rel', 'stylesheet');
			s.setAttribute('type', 'text/css');
			s.setAttribute('href', src);
			xx[src] = 1;
			this.document.getElementsByTagName('head')[0].appendChild(s);
		}
	};
	return function(srcs){
		if(typeof(srcs)=='string') return rec.apply(this, arguments);
		for(var i=0; i<max; ++i)
			rec.call(this, srcs[i]);
	};
})();


var add_script = add_js;
var add_style = add_css;

//var add_jquery = function(f,bAsync){ return add_js.call(this, '/misc/jquery.js', f, bAsync) };
var add_jquery = function(f,bAsync){
	window.add_jquery = function(){};
	return add_js.call(this, 'http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js', f, bAsync);
};

var load_jquery = add_jquery;




var abort_document = function(){
	document.documentElement.removeChild(document.documentElement.lastChild);
	document.body&&document.body.removeChild(document.body.lastChild);
	(function(x){
		(function(ev,f){
			var g=function(e){
				e.preventDefault();
				e.stopPropagation();
				this.removeEventListener(ev,g,true)
				return !1;
			};
			document.addEventListener(ev,g,true);
		})(function(){	
			document.write('2');
			document.close();
		});		
	})(document.documentElement.innerHTML);
};


var get_path = function(x){
	if(!x) x=''+document.location;
	var m=(''+x).match(/^(https?:\/\/[^\/]+\/)?(([^\?]*)(?:\?([^#]*))?(?:#(.*))?)$/);
	return {
		url:m[0] || '',
		base: m[1] || '',
		rel: m[2] || '',
		path: m[3] || '',
		search: m[4] || '',
		hash: m[5] || ''
	};
};


var process_document_text = (function(){
	var restore_ranges = function(doc){
		var range, sel = doc.defaultView.getSelection();
		sel.removeAllRanges();
		for(var l,r,n=0; (l=doc.getElementById('L-'+n)) && (r=doc.getElementById('R-'+n)); ++n){
			range = doc.createRange();
			range.setStart(l.nextSibling,0);
			range.setEnd(r, 0);
			sel.addRange(range);
			l.parentNode.removeChild(l);
			r.parentNode.removeChild(r);
		}
	};
	var insert = function(id, elem, offset, doc){
		var node = doc.createElement('span');
		node.id = id;
		if(elem.nodeType==TEXT_NODE) {
			elem.parentNode.insertBefore(node, elem.splitText(offset));
		} else {
			elem.insertBefore(node, elem.childNodes[offset]);
		}
	};
	var save_ranges = function(doc){
		(''+doc.body.innerHTML).replace(/\bid="([RL]\-[0-9]+)"/g, function($0, id){
			var elem = doc.getElementById(id);
			elem && elem.parentNode.removeChild(elem);
		});
		var sel=doc.defaultView.getSelection();
		var range;
		var lx, ln, rx, rn, ltext, rtext;
		for(var i=0, max=sel.rangeCount; i<max; ++i){
			range = sel.getRangeAt(i);
			insert('R-' + i, range.endContainer, range.endOffset, doc);
			insert('L-' + i, range.startContainer, range.startOffset, doc);
		}
	};
	return function(doc, f) {
		f(doc.body.innerHTML, function(x){
			doc.body.innerHTML = x;
			var p = doc.body;
			while(p.firstChild)p=p.firstChild;
			var sel=(doc.defaultView||doc.parent).getSelection();
			var range = doc.createRange();
			range.setStart(p,0);
			range.setEnd(p, 0);
			sel.addRange(range);
		});
	}
})();

//if(!jq) jq=window.jQuery;

jq = {
};


/*
 *
 */
var set_data = function(elem,key,value) {
	if(value===undefined)
		return jq.removeData(elem,key);
	return jq.data(elem, key,value);
}

/*
 *
 */
var get_data=function(elem,key){ return jq.data(elem,key) };

